from csv import reader, writer,DictWriter

with open('check.csv', "w") as file:
	csv_writer = writer(file)
	csv_writer.writerow(["Name", "attendance"])
	csv_writer.writerow(["Fred", "Present"])
	csv_writer.writerow(["Anna", "Absent"])

with open('check.csv', "r+") as file:
	csv_reader = reader(file)
	for row in file:
		print(row.upper())

# Version using DictWriter
with open("cats.csv", "w") as file:
	headers = ["Name", "Breed", "Age"]
	csv_writer = DictWriter(file, fieldnames=headers,delimiter="|")
	csv_writer.writeheader()
	csv_writer.writerow({
	    "Name": "Garfield",
	    "Breed": "Orange Tabby",
	    "Age": 10
	})
with open('cats.csv', "r+") as file:
	csv_reader = reader(file)
	for row in file:
		print(row)

  